<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $user = new \App\User();
       $user->name = 'player';
       $user->email = 'player@player.com';
       $user->password = bcrypt('qazwsx');
       $user->save();
    }
}

