<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('prize_type',['money','bonus_points','gift']);
            $table->string('prize_value');
            $table->enum('action_status',['pending','canceled','to_bank','to_bonus','convert_to_bonus','send_to_user']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_prizes');
    }
}
