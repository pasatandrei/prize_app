$(document).ready(function(){


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });

    //get price
    $(document.body).on('click','#get_prize', function () {
        $.ajax({
            url : '/prize/get_prize',
            type : "POST",
            success : function(response) {
                $('#prize').html(response);
            }
        });
    });

    $(document.body).on('click', '#go', function () {
        $.ajax({
            url : '/prize/save_action',
            type : "POST",
            data : {action : $('#action:selected').val()},
            dataType : 'json',
            success : function(response) {
                $('#prize').html('');
                if (response.bonus_points > 0) {
                    $('#bonus_points').html(response.bonus_points);
                }
            }
        });
    });




});
