<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/prize', [
   'uses' => 'PrizeController@index',
    'as' => 'prize.index'
]);

Route::post('/prize/get_prize',[
   'uses' => 'PrizeController@getPrize',
   'as' => 'prize.get_prize'
]);
Route::post('/prize/save_action', [
   'uses' => 'PrizeController@saveAction',
   'as' => 'prize.save_action'
]);
