<p>You win {{ $prize->value }} as {{ $prize->prize_type }}</p>
<div class="form-group">
    <label class="col-md-3">Chose action</label>
    <div class="col-md-9">
        <select id="action" class="form-control">
            @foreach(config('custom.prize_types')[$prize->prize_type]['actions'] as $action => $actionName)
                <option value="{{ $action }}">{{ $actionName }}</option>
            @endforeach
            <option value="canceled">Canceled</option>
        </select>
    </div>
</div>
<button id="go" class="btn btn-danger">Go</button>
