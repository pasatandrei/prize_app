@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <div class="col-md-12">Loyalty Account - <span id="bonus_points">{{ \Illuminate\Support\Facades\Auth::user()->bonus_points }}</span></div>
                        <hr>
                        <div class="col-md-12">
                            <button id="get_prize" class="btn badge-success">Get new prize!</button>
                        </div>
                        <hr>
                        <div class="clearfix"></div>
                        <div class="col-md-12" id="prize">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
