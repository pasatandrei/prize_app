<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Utils {

    public static function to_bank() {

        $user = Auth::user();
        //todo connect to Bank API and send money using user bank account

        return [
            'to_bank' => $user->pendingPrize->value
        ];

    }

    public static function convert_to_bonus() {
        $user = Auth::user();
        $user->bonus_points += (int)$user->pendingPrize->value * config('custom.coef');
        $user->save();

        return [
            'bonus_points' => $user->bonus_points
        ];
    }

    public static function to_bonus() {

        $user = Auth::user();
        $user->bonus_points += (int)$user->pendingPrize->value;
        $user->save();

        return [
            'bonus_points' => $user->bonus_points
        ];
    }

    public static function send_to_user() {
        //todo send to user
        return [
            'send_to_user' => true
        ];
    }

    public static function canceled() {
        return [
            'canceled' => true
        ];
    }


}
