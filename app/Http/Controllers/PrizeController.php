<?php

namespace App\Http\Controllers;

use App\Helpers\Utils;
use App\UsersPrize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrizeController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('prize.index');
    }

    public function getPrize(Request $request) {

        $user = Auth::user();

        $prize = new UsersPrize();
        $prize->user_id = $user->id;
        $prize->prize_type = array_random(array_keys(config('custom.prize_types')));
        $prize->value = config('custom.prize_types')[$prize->prize_type]['value'];
        $prize->action_status = 'pending';
        $prize->save();

        return view('prize.partial.prize',['prize' => $prize]);
    }

    public function saveAction(Request $request) {

        $user = Auth::user();

        if (!empty($user->pendingPrize)) {
            $action = $request['action'];
            $result = Utils::$request['action']();

            $user->pendingPrize->action = $action;
            $user->pendingPrize->save();

            return json_encode($result);
        } else {
            return json_encode(['error' => 'No active prize']);
        }

    }

}
