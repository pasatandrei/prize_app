<?php

return [
    'prize_types' => [
        'money' => [
            'actions' => [
                'to_bank' => 'Send to bank',
                'convert_to_bonus' => 'Convert to bonus points'
            ],
            'value' => rand(0,1000),
        ],
        'bonus_points' => [
            'actions' => [
                'to_bonus' => 'To bonus account'
            ],
            'value' => rand(0, 1000)
        ],
        'gift' => [
            'actions' => [
                'send_to_user' => 'Send to my address'
            ],
            'value' => array_random([
                'IPhone',
                'IPad',
                'MacBook'
            ])
        ]
    ],
    'coef' => 2

];
